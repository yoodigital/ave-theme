/**
 * @package 	WordPress
 * @subpackage 	Dental Clinic
 * @version 	1.0.4
 * 
 * Theme Information
 * Created by CMSMasters
 * 
 */

To update your theme please use the files list from the FILE LOGS below and substitute/add the listed files on your server with the same files in the Updates folder.

Important: after you have updated the theme, in your admin panel please proceed to
Theme Settings - Fonts and click "Save" in any tab,
then proceed to 
Theme Settings - Colors and click "Save" in any tab here.


-----------------------------------------------------------------------
				FILE LOGS
-----------------------------------------------------------------------


Version 1.0.4: files operations:

  Theme Files edited:
	dental-clinic\framework\admin\inc\plugin-activator.php
	dental-clinic\framework\admin\settings\cmsms-theme-settings-element.php
	dental-clinic\framework\function\theme-functions.php
	dental-clinic\framework\languages\dental-clinic.pot
	dental-clinic\readme.txt
	dental-clinic\style.css
	
	Proceed to wp-content\plugins\LayerSlider
	and update all files in this folder to version 5.6.9
	
	Proceed to wp-content\plugins\revslider
	and update all files in this folder to version 5.2.5.4


--------------------------------------

Version 1.0.3: files operations:

  Theme Files edited:
	dental-clinic\css\less\style.less
	dental-clinic\framework\admin\inc\admin-scripts.php
	dental-clinic\framework\admin\inc\css\jquery.cmsmsLightbox.css
	dental-clinic\framework\admin\inc\css\jquery.cmsmsLightbox-rtl.css
	dental-clinic\framework\admin\inc\js\jquery.cmsmsLightbox.js
	dental-clinic\framework\admin\inc\plugin-activator.php
	dental-clinic\framework\admin\options\cmsms-theme-options.php
	dental-clinic\framework\admin\options\cmsms-theme-options-general.php
	dental-clinic\framework\admin\options\cmsms-theme-options-other.php
	dental-clinic\framework\admin\options\cmsms-theme-options-page.php
	dental-clinic\framework\admin\options\cmsms-theme-options-post.php
	dental-clinic\framework\admin\options\cmsms-theme-options-profile.php
	dental-clinic\framework\admin\options\cmsms-theme-options-project.php
	dental-clinic\framework\admin\settings\cmsms-theme-settings.php
	dental-clinic\framework\class\likes-posttype.php
	dental-clinic\framework\class\widgets.php
	dental-clinic\framework\function\breadcrumbs.php
	dental-clinic\framework\function\template-functions-profile.php
	dental-clinic\framework\function\template-functions-project.php
	dental-clinic\framework\function\theme-functions.php
	dental-clinic\framework\languages\dental-clinic.pot
	dental-clinic\framework\postType\portfolio\post\gallery.php
	dental-clinic\framework\postType\portfolio\post\standard.php
	dental-clinic\framework\postType\portfolio\post\video.php
	dental-clinic\framework\postType\profile\post\standard.php
	dental-clinic\functions.php
	dental-clinic\js\jquery.isotope.mode.js
	dental-clinic\readme.txt
	dental-clinic\style.css

  Proceed to wp-content\plugins\cmsms-contact-form-builder 
	and update all files in this folder to version 1.3.6
	
	Proceed to wp-content\plugins\cmsms-content-composer 
	and update all files in this folder to version 1.5.2
	
	Proceed to wp-content\plugins\LayerSlider
	and update all files in this folder to version 5.6.8
	
	Proceed to wp-content\plugins\revslider
	and update all files in this folder to version 5.2.5.2


--------------------------------------

Version 1.0.2: files operations:

  Theme Files edited:
	dental-clinic\css\less\style.less
	dental-clinic\framework\admin\inc\plugin-activator.php
	dental-clinic\framework\function\template-functions.php
	dental-clinic\js\jquery.isotope.mode.js
	dental-clinic\js\jquery.script.js
	dental-clinic\readme.txt
	dental-clinic\style.css

  Theme Files replaced:
	dental-clinic\framework\admin\inc\plugins\LayerSlider.zip
	dental-clinic\framework\admin\inc\plugins\revslider.zip
	dental-clinic\framework\admin\inc\plugins\cmsms-mega-menu.zip
	dental-clinic\framework\admin\inc\plugins\cmsms-content-composer.zip
	dental-clinic\framework\admin\inc\plugins\cmsms-contact-form-builder.zip
	
	Proceed to wp-content\plugins\cmsms-contact-form-builder 
	and update all files in this folder to version 1.3.5
	
	Proceed to wp-content\plugins\cmsms-content-composer 
	and update all files in this folder to version 1.5.0
	
	Proceed to wp-content\plugins\cmsms-mega-menu 
	and update all files in this folder to version 1.2.6
	
	Proceed to wp-content\plugins\LayerSlider
	and update all files in this folder to version 5.6.6
	
	Proceed to wp-content\plugins\revslider
	and update all files in this folder to version 5.2.5


--------------------------------------

Version 1.0.1: files operations:

  Theme Files edited:
	dental-clinic\readme.txt
	dental-clinic\style.css
	dental-clinic\css\adaptive.css
	dental-clinic\css\less\adaptive.less
	dental-clinic\framework\function\theme-colors-primary.php
	dental-clinic\framework\function\theme-functions.php
	dental-clinic\js\jqueryLibraries.min.js


--------------------------------------
Version 1.0.0: Release!

